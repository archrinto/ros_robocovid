#include <ros/ros.h>
#include <robocovid_msgs/Command.h>
#include <sensor_msgs/Joy.h>

class Operator {
    public:
        Operator();

    private:
        void joy_callback(const sensor_msgs::Joy::ConstPtr& joy);

        int linearX_, linearY_, angularX_, angularY_, btnL1_, btnR1_, btnStart_;
        int l_scale_, a_scale_,s_scale_;

        robocovid_msgs::Command command;

        ros::NodeHandle nh;
        ros::Publisher command_pub;
        ros::Subscriber joy_sub;
};

Operator::Operator() {
    nh.param("axis_linearX", linearX_, linearX_);
    nh.param("axis_linearY", linearY_, linearY_);
    nh.param("axis_angularX", angularX_, angularX_);
    nh.param("axis_angularY", angularY_, angularY_);
    nh.param("btn_start", btnStart_, btnStart_);
    nh.param("btn_r1", btnR1_, btnR1_);
    nh.param("btn_l1", btnL1_, btnL1_);
    nh.param("scale_angular", a_scale_, a_scale_);
    nh.param("scale_linear", l_scale_, l_scale_);
    nh.param("scale_servo", s_scale_, s_scale_);

    joy_sub = nh.subscribe<sensor_msgs::Joy>("joy", 10, &Operator::joy_callback, this);
    command_pub = nh.advertise<robocovid_msgs::Command>("robocovid_command", 100);
}

void Operator::joy_callback(const sensor_msgs::Joy::ConstPtr& joy) {
    command.x_move.data = l_scale_ * joy->axes[linearX_];
    command.y_move.data = l_scale_ * joy->axes[linearY_];
    command.angle_move.data = a_scale_ * joy->axes[angularX_];
    command.y_camera_move.data = 0;
    command.x_camera_move.data = 0;

    if (joy->buttons[btnL1_]) {
        command.angle_move.data = 0;
        command.x_camera_move.data = joy->axes[angularX_];
        command.y_camera_move.data = joy->axes[angularY_];
    }

    command_pub.publish(command);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "robocovid_operator_node");

    Operator optr;

    ros::spin();
    return 0;
}
