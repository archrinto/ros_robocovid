#!/usr/bin/env python
import rospy
from robocovid_msgs.msg import Command
import socket
import time
import json
import gi
gi.require_version("Gst", "1.0")
gi.require_version("GstApp", "1.0")
from gi.repository import Gst
from gi.repository import GObject
from gi.repository import GstApp
from threading import Thread
import os
import cv2
import numpy as np
# from Tkinter import Tk, Label
# import tkMessageBox
import struct
import fcntl
import array


device_video = 2
device_audio = 2
device_videocall = 0
device_audiocall = 2


def all_interfaces():
    max_possible = 128  # arbitrary. raise if needed.
    bytes = max_possible * 32
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    names = array.array('B', '\0' * bytes)
    outbytes = struct.unpack('iL', fcntl.ioctl(
        s.fileno(),
        0x8912,  # SIOCGIFCONF
        struct.pack('iL', bytes, names.buffer_info()[0])
    ))[0]
    namestr = names.tostring()
    lst = []
    for i in range(0, outbytes, 40):
        name = namestr[i:i+16].split('\0', 1)[0]
        ip   = namestr[i+20:i+24]
        lst.append((name, ip))
    return lst


def format_ip(addr):
    return str(ord(addr[0])) + '.' + \
           str(ord(addr[1])) + '.' + \
           str(ord(addr[2])) + '.' + \
           str(ord(addr[3]))


def update_ip_address(label):
    def update():
        ifs = all_interfaces()
        ipaddr = ""
        for i in ifs:
            ipaddr += "%12s   %s" % (i[0], format_ip(i[1]))
            ipaddr += "\n"

        label.config(text=str(ipaddr))
        label.after(1000, update)
    update()


def show_address_info():
    root = Tk()
    root.geometry("300x100")
    root.title("IP Address Info")

    eth1 = Label(root, text="Listen on: ")
    eth1.pack()
    update_ip_address(eth1)

    root.mainloop()


# guithread = Thread(target=show_address_info)
# guithread.daemon = True
# guithread.start()

# _ = GstApp

path = os.path.dirname(os.path.abspath(__file__))

print(path)

GObject.threads_init()
Gst.init(None)


class AudioPlayer:
    def __init__(self):
        self.loop = GObject.MainLoop()
        self.pipeline = None

    def play(self, file):
        print(file)
        self.pipeline = Gst.parse_launch("filesrc location={} ! mpegaudioparse ! mpg123audiodec ! audioconvert ! audioresample ! alsasink".format(file))

        self.bus = self.pipeline.get_bus()
        self.bus.add_signal_watch()
        self.bus.connect("message", self.on_message)

        # self.run()
        print("play audio")
        self.thread = Thread(target=self.run, args=())
        self.thread.daemon = True
        self.thread.start()

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)

        try:
            self.loop.run()
        except Exception as e:
            print(e)
            self.loop.quit()

        print("done play")
        self.pipeline.set_state(Gst.State.NULL)

    def on_message(self, bus, message):
        t = message.type
        if t == Gst.MessageType.EOS:
            self.pipeline.set_state(Gst.State.NULL)
            self.loop.quit()
        elif t == Gst.MessageType.ERROR:
            self.pipeline.set_state(Gst.State.NULL)
            self.loop.quit()

    def stop(self):
        if self.pipeline is not None:
            self.pipeline.set_state(Gst.State.NULL)

        self.loop.quit()


class VideoStream:
    def __init__(self):
        self.pipeline = None
        self.loop = GObject.MainLoop()
        self.thread = None

    def main(self, ip, device_video, device_audio):
        device_video = "/dev/video{}".format(device_video)
        device_audio = "hw:{},0".format(device_audio)
        print(device_video)
        self.pipeline = Gst.parse_launch("v4l2src device={} ! video/x-raw, format=YUY2,width=640,height=480 ! videoconvert ! videoscale ! video/x-raw,width=320,height=240 ! x264enc tune=zerolatency bitrate=500 speed-preset=superfast ! rtph264pay ! udpsink host={} port=5000 alsasrc device={} ! audioconvert ! audioresample  ! alawenc ! rtppcmapay ! udpsink host={} port=5001".format(device_video, ip, device_audio, ip))
        print("v4l2src device={} ! video/x-raw, format=YUY2,width=640,height=480 ! videoconvert ! x264enc tune=zerolatency bitrate=500 speed-preset=superfast ! rtph264pay ! udpsink host={} port=5000 alsasrc device={} ! audioconvert ! audioresample  ! alawenc ! rtppcmapay ! udpsink host={} port=5001".format(device_video, ip, device_audio, ip))


    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)

        try:
            self.loop.run()
        except Exception as e:
            print(e)
            self.loop.quit()

        print("strem ended")
        self.pipeline.set_state(Gst.State.NULL)

    def start(self, ip="127.0.0.1", device_video="0", device_audio="1"):
        self.main(ip, device_video, device_audio)
        print("strem start ", ip)
        self.thread = Thread(target=self.run, args=())
        self.thread.daemon = True
        self.thread.start()

    def stop(self):
        if self.pipeline is not None:
            self.pipeline.set_state(Gst.State.NULL)

        self.loop.quit()


class VideoCall:
    def __init__(self):
        self.pipeline = Gst.parse_launch("udpsrc port=6000 ! h264parse ! avdec_h264 ! videoconvert ! videoscale ! video/x-raw,width=640,height=360 ! autovideosink sync=false udpsrc port=6001 ! rawaudioparse use-sink-caps=false format=pcm pcm-format=s16le sample-rate=8000 num-channels=1 ! queue ! audioconvert ! audioresample ! alsasink")
        self.appsink = self.pipeline.get_by_name("videocall")
        self.loop = GObject.MainLoop()
        self.thread = None
        self.active = False

    def start(self):
        print('start video call')
        self.active = True
        # self.thread = Thread(target=self.loop.run, args=())
        # self.thread.daemon = True
        # self.thread.start()
        self.thread_sink = Thread(target=self.run, args=())
        self.thread_sink.daemon = True
        self.thread_sink.start()

    def run(self):
        self.pipeline.set_state(Gst.State.PLAYING)

        try:
            self.loop.run()
        except Exception as e:
            print(e)
            self.loop.quit()

        # try:
        #     while self.active:
        #         sample = self.appsink.try_pull_sample(Gst.SECOND)
        #         if sample is None:
        #             continue
                
        #         buff = sample.get_buffer()
        #         size, offset, maxsize = buff.get_sizes()
        #         frame_data = buff.extract_dup(offset, size) 

        #         img_array = np.asarray(bytearray(frame_data), dtype=np.uint8)

        #         print(img_array.shape)
        # except Exception as e:
        #     print(e)

        print("strem ended")
        self.pipeline.set_state(Gst.State.NULL)
        self.loop.quit()

    def stop(self):
        print('stop video call')
        if self.pipeline is not None:
            self.pipeline.set_state(Gst.State.NULL)

        self.loop.quit()
        self.active = False


videostream = VideoStream()
audioplayer = AudioPlayer()
videocall = VideoCall()

# start ros node
rospy.init_node('robocovid_command')
rospy.loginfo('robocovid_command node started')

pub = rospy.Publisher('robocovid_command', Command, queue_size=1)

# socket to communicate with android
serv = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
port = 54321
serv.bind(('0.0.0.0', port))
serv.listen(5)

print(socket.gethostbyname(socket.gethostname()))
print("server listen on", port)

while True:
    conn, addr = serv.accept()
    print(addr)

    while True:
        point = {'x': 0, 'y': 0, 'z': 0}
        try:
            data = conn.recv(4096)

            if not data:
                break

            data_str = data.decode("utf-8")
            data_json = json.loads(data_str)

            if data_json['cmd'] == "connect":
                videostream.stop()
                videostream.start(addr[0], device_video, device_audio)
            elif data_json['cmd'] == "move":
                point = data_json
            elif data_json['cmd'] == "disconnect":
                videostream.stop()
            elif data_json['cmd'] == "bell":
                audioplayer.stop()
                audioplayer.play(path + '/bell.mp3')
            elif data_json['cmd'] == "horn":
                audioplayer.stop()
                audioplayer.play(path + '/horn.mp3')
            elif data_json['cmd'] == "start_video_call":
                videocall.start()
                videostream.stop()
                videostream.start(addr[0], device_videocall, device_audiocall)
            elif data_json['cmd'] == "stop_video_call":
                videocall.stop()
                videostream.stop()
                videostream.start(addr[0], device_video, device_audio)

            conn.send("copy that!".encode("utf-8"))
            # time.sleep(0.01)  # detik

        except Exception as e:
            print(e)

        command_msgs = Command()
        command_msgs.x_move.data = point['x']
        command_msgs.y_move.data = point['y']
        command_msgs.angle_move.data = point['z']

        pub.publish(command_msgs)

    conn.close()
    print('client disconnected')
