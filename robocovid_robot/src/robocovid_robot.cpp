#include <ros/ros.h>
#include <robocovid_msgs/Command.h>
#include <robocovid_msgs/Motion.h>
#include <robocovid_msgs/IK.h>

#define PI 3.14159265

class Robot {
    public: 
        Robot();

    private: 
        const float R = 0.24; // m
        const float D_roda = 15.0; // cm
        const float K_robot = PI * R * 2; // m
        const float K_roda = PI * D_roda; // cm

        ros::NodeHandle nh;
        ros::Subscriber command_sub;
        ros::Publisher motion_pub;

        robocovid_msgs::IK ik_srv;
        robocovid_msgs::Motion motion;

        bool inverst_kinematic(
            robocovid_msgs::IK::Request &req,
            robocovid_msgs::IK::Response &res
        );
        void command_callback(const robocovid_msgs::Command& command);

};

Robot::Robot() {
    motion_pub = nh.advertise<robocovid_msgs::Motion>("/robocovid_motion", 100);
    command_sub = nh.subscribe("robocovid_command", 10, &Robot::command_callback, this);
}

bool Robot::inverst_kinematic(
  robocovid_msgs::IK::Request &req,
  robocovid_msgs::IK::Response &res
) {
  float v1 = -0.5 * req.dis_x.data + 0.8660 * req.dis_y.data + R * req.dis_theta.data;
  float v3 = -0.5 * req.dis_x.data - 0.8660 * req.dis_y.data + R * req.dis_theta.data;
  float v2 = 1.0 * req.dis_x.data + R * req.dis_theta.data;

  // mengembalikan nilai
  res.rpm1.data = v1; 
  res.rpm2.data = v2; 
  res.rpm3.data = v3; 

  return true;
}

void Robot::command_callback(const robocovid_msgs::Command& command) {
    ik_srv.request.dis_x.data = command.x_move.data;
    ik_srv.request.dis_y.data = command.y_move.data;
    ik_srv.request.dis_theta.data = command.angle_move.data;

    inverst_kinematic(ik_srv.request, ik_srv.response);

    motion.rpm1.data = ik_srv.response.rpm1.data;
    motion.rpm2.data = ik_srv.response.rpm2.data;
    motion.rpm3.data = ik_srv.response.rpm3.data;
    motion.servo1.data = command.x_camera_move.data;
    motion.servo2.data = command.y_camera_move.data;

    motion_pub.publish(motion);
}

int main(int argc, char **argv)
{
    ros::init(argc, argv, "robocovid_robot_node");

    Robot robot;

    ros::spin();
    return 0;
}
